package com.itb.tictactoe

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toDrawable
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket

/**
 * Created by Jordi Garcia & Noah Alerm (The putos best) on 29/4/2022.
 */
class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var cell1: ImageView
    private lateinit var cell2: ImageView
    private lateinit var cell3: ImageView
    private lateinit var cell4: ImageView
    private lateinit var cell5: ImageView
    private lateinit var cell6: ImageView
    private lateinit var cell7: ImageView
    private lateinit var cell8: ImageView
    private lateinit var cell9: ImageView
    private var player: Boolean = false
    private var end = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cell1 = findViewById(R.id.imageView)
        cell2 = findViewById(R.id.imageView2)
        cell3 = findViewById(R.id.imageView3)
        cell4 = findViewById(R.id.imageView4)
        cell5 = findViewById(R.id.imageView5)
        cell6 = findViewById(R.id.imageView6)
        cell7 = findViewById(R.id.imageView7)
        cell8 = findViewById(R.id.imageView8)
        cell9 = findViewById(R.id.imageView9)

        cell1.setOnClickListener(this)
        cell2.setOnClickListener(this)
        cell3.setOnClickListener(this)
        cell4.setOnClickListener(this)
        cell5.setOnClickListener(this)
        cell6.setOnClickListener(this)
        cell7.setOnClickListener(this)
        cell8.setOnClickListener(this)
        cell9.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        val iv = v as ImageView
        if (iv.drawable.constantState?.equals(resources.getDrawable(R.drawable.square).constantState) == true) {
            when (v.id) {
                R.id.imageView -> sendMessage(1, iv)
                R.id.imageView2 -> sendMessage(2, iv)
                R.id.imageView3 -> sendMessage(3, iv)
                R.id.imageView4 -> sendMessage(4, iv)
                R.id.imageView5 -> sendMessage(5, iv)
                R.id.imageView6 -> sendMessage(6, iv)
                R.id.imageView7 -> sendMessage(7, iv)
                R.id.imageView8 -> sendMessage(8, iv)
                R.id.imageView9 -> sendMessage(9, iv)
            }
        }
    }

    private fun sendMessage(msg: Int, iv: ImageView) {
        val handler = Handler()
        val thread = Thread {
            try {
                val s = Socket("172.23.5.102", 50000)
                val out = s.getOutputStream()
                val output = PrintWriter(out)
                output.println(msg)
                output.flush()
                val input = BufferedReader(InputStreamReader(s.getInputStream()))
                val st = input.readLine()
                handler.post {
                    if (st != "")
                        Log.d("From server", st)
                    if (!player)
                        iv.setImageResource(R.drawable.cross)
                    else if (player)
                        iv.setImageResource(R.drawable.circle)

                    player = !player
                }

                output.close()
                out.close()
                s.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        thread.start()
    }
}